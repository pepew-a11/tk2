from django.test import TestCase, Client
from .models import RumahSakit

# Create your tests here.

class PenangananTest(TestCase):

    def test_index_url_exists(self):
        response = Client().get('/penanganan/')
        self.assertEquals(response.status_code, 200)

    def test_index_template_exists(self):
        response = Client().get('/penanganan/')
        self.assertTemplateUsed(response, 'penanganan/index.html')
    
    def test_add_check_button_exists(self):
        response = Client().get('/penanganan/')
        html_response = response.content.decode('utf8')
        self.assertIn("Tambah RS", html_response)
        self.assertIn("check", html_response)

class CheckTest(TestCase):

    def test_user_check(self):
        new = RumahSakit.objects.create(nama="RS Kencana", hp="089615356534", alamat="Jl. jl kuy", provinsi="aceh")

        response = Client().post('/penanganan/', {'provinsi':'aceh'})
        html_response = response.content.decode('utf8')
        self.assertIn('RS Kencana', html_response)

class AddTest(TestCase):

    def test_add_url_exists(self):
        response = Client().get('/penanganan/add')
        self.assertEquals(response.status_code, 200)

    def test_add_template_exists(self):
        response = Client().get('/penanganan/add')
        self.assertTemplateUsed(response, 'penanganan/add.html')

    # def test_add_working(self):
    #     Client().post('/penanganan/add', {'nama':'RS Kencana', 'hp':'089615356534', 'alamat':'Jl. jl kuy', 'provinsi':'aceh'})

    #     response = Client().post('/penanganan', {'provinsi':'aceh'})
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('RS Kencana', html_response)

class DeleteTest(TestCase):

    def test_delete_working(self):
        new = RumahSakit.objects.create(nama="RS Kencana", hp="089615356534", alamat="Jl. jl kuy", provinsi="aceh")
        
        self.assertEquals(RumahSakit.objects.all().count(), 1)
        Client().get('/penanganan/delete/' + str(new.id))

        self.assertEquals(RumahSakit.objects.all().count(), 0)


