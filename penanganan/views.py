from django.shortcuts import render, redirect
from .models import RumahSakit
from .forms import FormRS, FormProvinsi

# Create your views here.

def index(request):
    if request.method == "POST":
        form = FormProvinsi(request.POST)
        if form.is_valid():
            provinsi = form.cleaned_data['provinsi']
            list_rumah_sakit = RumahSakit.objects.filter(provinsi = provinsi)
            context = {
                'list_rumah_sakit' : list_rumah_sakit,
                'form' : FormProvinsi()}

            return render(request, 'penanganan/index.html', context)
    
    else:
        form = FormProvinsi()
        return render(request, 'penanganan/index.html', {'form' : form})

def rumah_sakit(request):
    list_rumah_sakit = RumahSakit.objects.all().filter(provinsi = request.POST['provinsi'])
    
    context =   {"list_rumah_sakit" : list_rumah_sakit}

    return render(request, 'penanganan/rumah_sakit.html', context)

def add(request):
    if request.method == "POST":
        form = FormRS(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            hp = form.cleaned_data['hp']
            alamat = form.cleaned_data['alamat']
            provinsi = form.cleaned_data['provinsi']

            newRS = RumahSakit(nama=nama, hp=hp, alamat=alamat, provinsi=provinsi)
            newRS.save()

            return redirect('../')
    
    else:
        form = FormRS()

        return render(request, 'penanganan/add.html', {"form" : form})

def delete(request, id):
    if request.method == "GET":
        rumah_sakit = RumahSakit.objects.get(id = id)
        rumah_sakit.delete()

        return redirect('../')
    
    return redirect('../')

