from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string

from .models import Info
from .forms import InfoForm

import requests
import json

# Create your views here.

def index(request):
    form = InfoForm()
    update = render_to_string('hoaxbuster/infonobutton.html', {'infos' : Info.objects.all()})

    if request.user.is_authenticated:
        update = render_to_string('hoaxbuster/info.html', {'infos' : Info.objects.all()})

    context = {'info' : update, 'form' : form}
    
    return render(request, 'hoaxbuster/index.html', context)

def updateinfo(request, info_id):
    info = Info.objects.get(id=info_id)
    form = InfoForm(instance=info)
    context = {'form' : form}

    if request.method == 'POST':
        form = InfoForm(request.POST, request.FILES, instance=info)
  
        if form.is_valid():
            form.save()
        
        return redirect('/hoaxbuster/')

    return render(request, 'hoaxbuster/updateinfo.html', context)

def addinfo(request):
    form = InfoForm()
    context = {'form' : form}

    if request.method == 'POST':
        form = InfoForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
        
        update = render_to_string('hoaxbuster/infonobutton.html', {'infos' : Info.objects.all()})

        if request.user.is_authenticated:
            update = render_to_string('hoaxbuster/info.html', {'infos' : Info.objects.all()})

        return JsonResponse({'update' : update})
        # return redirect('/hoaxbuster/')

    # return render_to_string('hoaxbuster/info.html', {'infos' : Info.objects.all()})
    return render(request, 'hoaxbuster/addinfo.html', context)

def deleteinfo(request, info_id):
    info = Info.objects.get(id=info_id)
    info.delete()

    update = render_to_string('hoaxbuster/infonobutton.html', {'infos' : Info.objects.all()})

    if request.user.is_authenticated:
        update = render_to_string('hoaxbuster/info.html', {'infos' : Info.objects.all()})

    return JsonResponse({'update' : update})

def data(request):
    url = "https://factchecktools.googleapis.com/v1alpha1/claims:search?languageCode=" + request.GET['l'] + "&query=" + request.GET['q'] + "&key=AIzaSyD__RG_NfDmqJ8o9Yqr5qefnrw8R7QEyyQ"
    ret = requests.get(url)

    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)