$(document).ready( function() {
    $("#keyword").keyup( function() {
        var input = $(this).val();
        var language = $(this).parent().parent().find("#language option:selected").val();
        console.log(language);
        console.log(input);
    
        $.ajax({
            url: '/hoaxbuster/data?l=' + language + '&q=' + input,
            success: function(data) {
                var array_items = data.claims;
                console.log(array_items);
                $("#search-result").empty();
                for (i = 0; i < array_items.length; i++) {
                    var title = array_items[i].text;
                    var publisher = array_items[i].claimReview[0].publisher.name;
                    var description = array_items[i].claimReview[0].textualRating;
                    $("#search-result").append("<tr><td>" + publisher + "</td><td>" + title + "</td><td>" + description + "</td></tr>");
                }
            }
        });
    });

    $("#info-form").submit( function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);

        $.ajax({
            type: "POST",
            url: "addinfo",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            enctype: 'multipart/form-data',
            processData: false,
            // data: {
            //     hoax: $('textarea#id_hoax').val(),
            //     fakta: $('textarea#id_fakta').val(),
            //     image: $('input:file[name=image]').val(),
            //     csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            // },
            success: function(response){
                console.log(response);
                $('textarea#id_hoax').val('');
                $('textarea#id_fakta').val('');
                $('input#id_image').val('');
                $("#message").html("succesfully added");
                $("#info").html(response.update);
            }
        });
    });

    $("#info").click( function(event) {
        console.log("in")

        if ($(event.target).hasClass('del-btn')) {
            $.ajax({
                type: "GET",
                url: "deleteinfo/" + $(event.target).val(),
    
                success: function(response){
                    $("#info").html(response.update);
                }
            });
        }
    });
});