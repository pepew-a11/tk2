$("#keyword").keyup( function() {
    var input = $(this).val();
    var language = $(this).parent().parent().find("#language option:selected").val();
    console.log(language);
    console.log(input);

    $.ajax({
        url: '/hoaxbuster/data?l=' + language + '&q=' + input,
        success: function(data) {
            var array_items = data.claims;
            console.log(array_items);
            $("#search-result").empty();
            for (i = 0; i < array_items.length; i++) {
                var title = array_items[i].text;
                var publisher = array_items[i].claimReview[0].publisher.name;
                var description = array_items[i].claimReview[0].textualRating;
                $("#search-result").append("<tr><td>" + publisher + "</td><td>" + title + "</td><td>" + description + "</td></tr>");
            }
        }
    });
});
