from django.shortcuts import render,redirect
from .models import Data
from .forms import DataForm

# Create your views here.

def index(request):
    data = Data.objects.all()
    context = {'data':data}
    return render(request, 'home/index.html',context)

def indexubah(request):
    form = DataForm()
    if request.method == 'POST':
        form = DataForm(request.POST)
        if form.is_valid():
            Data.objects.all().delete()
            form.save()
            return redirect('/')
    context = {'form':form}
    return render(request,'home/indexubah.html',context)

