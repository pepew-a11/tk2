from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from .models import *
from .FAQForm import Content
from django.template.loader import render_to_string

from django.contrib.auth.decorators import login_required

# Create your views here.

def index(request):
     form = Content()
     contents = FAQContent.objects.all()

     return render(request, 'faq/index.html', {'forms' : form , 'contents' : contents})

#@login_required(login_url="tk2/userauth/")
def Add(request):
    form = Content()
    if request.is_ajax and request.method == 'POST':
        form = Content(request.POST)
        if form.is_valid():
            instance = form.save()
            return redirect('/')
        else:
            # some form errors occured.
            return JsonResponse({"error": form.errors}, status=400)

    return render(request, 'faq/Add.html',{'form' : form})

#@login_required(login_url="tk2/userauth/")
def Delete(request, pk):
    konten = FAQContent.objects.filter(id=pk)
    judul = FAQContent.objects.get(id= pk)
    if request.method =='POST':
        konten.delete()
        return redirect('/faq/')
    context = {'form':konten}
    return render(request, 'faq/index.html' , context)

#@login_required(login_url="tk2/userauth/")
def Edit(request, idx):
    content = FAQContent.objects.get(id=idx)
    form = Content(instance=content)

    context= {'form': form}

    if request.method == 'POST': 
        form = Content(request.POST, request.FILES, instance=content)
  
        if form.is_valid():
            form.save()
        
        return redirect('/faq/')

    return render(request, 'faq/Edit.html', context)

def data(request):
    url = "https://faq.coronavirus.gov/api/v2/questions.json"
    ret = requests.get(url)

    contentData = json.loads(ret.content)
    return JsonResponse(contentData, safe = False)

    